# fxapp 

A simple foreign exchange application covering the following requirements:    
**Functional Requirements:**   
1. Exchange Rate API    
○ input : currency pair to retrieve the exchange rate    
○ output : exchange rate    
2. Conversion API:    
○ input: source amount, source currency, target currency    
○ output: amount in target currency, and transaction id.    
3. Conversion List API    
○ input: transaction id or transaction date (at least one of the inputs shall
be provided for each call)    
○ output: list of conversions filtered by the inputs and paging is required    
4. The application shall use a service provider to retrieve exchange rates and
optionally for calculating amounts.    
5. In the case of an error, a specific code to the error and a meaningful message
shall be provided.      

**Technical Requirements:**     
1. Application shall be run without need of extra configuration.   
2. APIs shall be developed as RESTful APIs with Spring Boot.   
3. Build & dependency management tools shall be used.   
4. Design patterns shall be used when suitable.   
5. Code structure (i.e. packages) shall reflect the separation of concerns.   
6. Unit tests shall be provided.    

**Optional**   
1. Develop a central error logging mechanism for the application.   
2. API Documentation shall be provided.   
     
      
     
To run the application you should:
1. Clone the project Git repository.
2. Build and run the applicataion
     
     
The REST endpoints are documented with Swagger. Check at: [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) while the application is running.
