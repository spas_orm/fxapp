package com.spasorm.fxapp.controllers;

import com.spasorm.fxapp.services.RatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class RateController {

    RatesService ratesService;

    @Autowired
    public RateController(RatesService ratesService) {
        this.ratesService = ratesService;
    }

    @GetMapping("/api/v1/rate")
    public BigDecimal getRate(@RequestParam(required = true) String fromCurrency,
                              @RequestParam(required = true) String toCurrency) {
        return ratesService.getExchangeRate(fromCurrency, toCurrency);
    }

}
