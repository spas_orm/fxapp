package com.spasorm.fxapp.controllers;

import com.spasorm.fxapp.models.Transaction;
import com.spasorm.fxapp.models.dtos.TransactionInputDTO;
import com.spasorm.fxapp.models.dtos.TransactionOutputDTO;
import com.spasorm.fxapp.services.TransactionsService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;


@RestController
public class TransactionsController {

    TransactionsService transactionsService;

    public TransactionsController(TransactionsService transactionsService) {
        this.transactionsService = transactionsService;
    }

    @PostMapping("/api/v1/convert")
    public TransactionOutputDTO convertCurrency(@RequestBody TransactionInputDTO transactionInputDTO) {

        return transactionsService.create(transactionInputDTO);

    }

    @GetMapping("/api/v1/transaction")
    public Page<Transaction> getTransactionsForParameters(@RequestParam(name = "id", required = false, defaultValue = "0") int id,
                                                          @RequestParam(name = "date", required = false) String date,
                                                          @PageableDefault(size = 5) Pageable pageable) {
        return transactionsService.getTransactionsByIdOrDate(id, date, pageable);
    }

    @GetMapping("/api/v1/transaction/{id}")
    public Transaction getById(@PathVariable int id) {

        return transactionsService.getById(id);

    }

    @GetMapping("/api/v1/transaction/date/{date}")
    public Page<Transaction> getByDate(@PathVariable String date, @PageableDefault(size = 5) Pageable pageable) {

        return transactionsService.getByDate(date, pageable);
    }

    @GetMapping("/api/v1/transaction/list")
    public Page<Transaction> getAll(@PageableDefault(size = 5) Pageable pageable) {

        return transactionsService.getAll(pageable);
    }
}
