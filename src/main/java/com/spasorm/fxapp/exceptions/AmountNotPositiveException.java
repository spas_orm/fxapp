package com.spasorm.fxapp.exceptions;

public class AmountNotPositiveException extends RuntimeException {

    public AmountNotPositiveException() {
    }

    public AmountNotPositiveException(String message) {
        super(message);
    }
}
