package com.spasorm.fxapp.exceptions;

public class InvalidDateFormatException extends RuntimeException {
    public InvalidDateFormatException() {
    }

    public InvalidDateFormatException(String message) {
        super(message);
    }
}
