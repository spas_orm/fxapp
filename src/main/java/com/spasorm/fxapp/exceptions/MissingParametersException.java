package com.spasorm.fxapp.exceptions;

public class MissingParametersException extends RuntimeException {
    private String message;

    public MissingParametersException() {
    }

    public MissingParametersException(String message) {
        super(message);
    }

}
