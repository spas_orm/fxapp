package com.spasorm.fxapp.exceptions;

public class ThirdPartyProviderConnectionException extends RuntimeException {

    public ThirdPartyProviderConnectionException() {
    }

    public ThirdPartyProviderConnectionException(String message) {
        super(message);
    }

}
