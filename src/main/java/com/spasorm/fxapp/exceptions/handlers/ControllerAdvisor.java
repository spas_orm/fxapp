package com.spasorm.fxapp.exceptions.handlers;

import com.spasorm.fxapp.exceptions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {
    public static final String CURRENCY_NOT_AVAILABLE = "The the requested currency is not available";
    public static final String INVALID_AMOUNT = "Transaction amount must be positive";
    public static final String EXTERNAL_SERVICE_ISSUE = "Third party provider connection issue";
    public static final String INVALID_DATE_FORMAT = "You have entered an invalid date. [Required format: date=YYYY-MM-DD]";
    public static final String TRANSACTION_NOT_FOUND = "The requested transaction does not exist";
    public static final String MISSING_QUERY_PARAMETERS = "Missing query parameters! Please provide value for ID and/or DATE";

    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerAdvisor.class);

    @ExceptionHandler(AmountNotPositiveException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = INVALID_AMOUNT)
    public void handleAmountNotPositiveException(AmountNotPositiveException ex) {
        LOGGER.error("Error: {} \n{} \n{}", INVALID_AMOUNT, ex.getClass().toString(), Arrays.toString(ex.getStackTrace()));
    }

    @ExceptionHandler(CurrencyNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = CURRENCY_NOT_AVAILABLE)
    public void handleCurrencyNotFoundException(CurrencyNotFoundException ex) {
        LOGGER.error("Error: {} \n {} \n {}", CURRENCY_NOT_AVAILABLE, ex.getClass().toString(), Arrays.toString(ex.getStackTrace()));
    }

    @ExceptionHandler(InvalidDateFormatException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = INVALID_DATE_FORMAT)
    public void handleInvalidDateFormatException(InvalidDateFormatException ex) {
        LOGGER.error("Error: {} \n {} \n {}", INVALID_DATE_FORMAT, ex.getClass().toString(), Arrays.toString(ex.getStackTrace()));
    }

    @ExceptionHandler(MissingParametersException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = MISSING_QUERY_PARAMETERS)
    public void handleMissingParametersException(MissingParametersException ex) {
        LOGGER.error("Error: {} \n {} \n {}", MISSING_QUERY_PARAMETERS, ex.getClass().toString(), Arrays.toString(ex.getStackTrace()));
    }

    @ExceptionHandler(ThirdPartyProviderConnectionException.class)
    @ResponseStatus(value = HttpStatus.FAILED_DEPENDENCY, reason = EXTERNAL_SERVICE_ISSUE)
    public void handleThirdPartyProviderConnectionException(ThirdPartyProviderConnectionException ex) {
        LOGGER.error(EXTERNAL_SERVICE_ISSUE);
        LOGGER.error("Error: {} \n {} \n {}", EXTERNAL_SERVICE_ISSUE, ex.getClass().toString(), Arrays.toString(ex.getStackTrace()));
    }

    @ExceptionHandler(TransactionNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = TRANSACTION_NOT_FOUND)
    public void handleTransactionNotFoundException(TransactionNotFoundException ex) {
        LOGGER.error(TRANSACTION_NOT_FOUND);
        LOGGER.error("Error {} \n {} \n {}", TRANSACTION_NOT_FOUND, ex.getClass().toString(), Arrays.toString(ex.getStackTrace()));
    }

}
