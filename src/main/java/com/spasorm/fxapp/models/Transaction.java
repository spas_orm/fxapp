package com.spasorm.fxapp.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name="TRANSACTIONS")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TRANSACTION_ID", updatable = false, nullable = false)
    private int id;

    @Column(name = "SOURCE_CURRENCY",nullable = false)
    private String sourceCurrency;

    @Column(name = "SOURCE_AMOUNT", nullable = false, scale = 6)
    private BigDecimal sourceAmount;

    @Column(name = "TARGET_CURRENCY", nullable = false)
    private String targetCurrency;

    @Column(name = "TARGET_AMOUNT", nullable = false, scale = 6)
    private BigDecimal targetAmount;

    @Column(name = "RATE", nullable = false, precision = 14, scale = 6)
    private BigDecimal rate;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "DATE", nullable = false)
    private LocalDate date;

    public Transaction() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSourceCurrency() {
        return sourceCurrency;
    }

    public void setSourceCurrency(String sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    public BigDecimal getSourceAmount() {
        return sourceAmount;
    }

    public void setSourceAmount(BigDecimal sourceAmount) {
        this.sourceAmount = sourceAmount;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrencyId) {
        this.targetCurrency = targetCurrencyId;
    }

    public BigDecimal getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(BigDecimal targetAmount) {
        this.targetAmount = targetAmount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
