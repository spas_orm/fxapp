package com.spasorm.fxapp.models.dtos;

import java.math.BigDecimal;

public class TransactionInputDTO {

    private BigDecimal sourceAmount;
    private String sourceCurrency;
    private String targetCurrency;

    public TransactionInputDTO() {
    }

    public BigDecimal getSourceAmount() {
        return sourceAmount;
    }

    public void setSourceAmount(BigDecimal sourceAmount) {
        this.sourceAmount = sourceAmount;
    }

    public String getSourceCurrency() {
        return sourceCurrency;
    }

    public void setSourceCurrency(String sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;
    }
}
