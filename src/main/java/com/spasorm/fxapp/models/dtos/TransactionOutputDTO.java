package com.spasorm.fxapp.models.dtos;

import java.math.BigDecimal;

public class TransactionOutputDTO {

    private BigDecimal targetAmount;
    private int transactionId;

    public TransactionOutputDTO() {
    }

    public BigDecimal getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(BigDecimal targetAmount) {
        this.targetAmount = targetAmount;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }
}
