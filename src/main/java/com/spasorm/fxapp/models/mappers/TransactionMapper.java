package com.spasorm.fxapp.models.mappers;

import com.spasorm.fxapp.models.Transaction;
import com.spasorm.fxapp.models.dtos.TransactionInputDTO;
import com.spasorm.fxapp.models.dtos.TransactionOutputDTO;
import com.spasorm.fxapp.services.RatesService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;

@Component
public class TransactionMapper {

    public TransactionMapper() {
    }

    public Transaction inputDtoToTransaction(TransactionInputDTO transactionInDto) {
        Transaction transaction = new Transaction();

        transaction.setSourceAmount(transactionInDto.getSourceAmount());
        transaction.setSourceCurrency(transactionInDto.getSourceCurrency());
        transaction.setTargetCurrency(transactionInDto.getTargetCurrency());
        transaction.setRate(BigDecimal.ZERO);
        transaction.setTargetAmount(BigDecimal.ZERO);
        transaction.setDate(LocalDate.now());

        return transaction;
    }

    public TransactionOutputDTO transactionToOutDto(Transaction transaction) {
        TransactionOutputDTO transactionOutDTO = new TransactionOutputDTO();

        transactionOutDTO.setTargetAmount(transaction.getTargetAmount());
        transactionOutDTO.setTransactionId(transaction.getId());

        return transactionOutDTO;

    }
}
