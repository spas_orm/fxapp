package com.spasorm.fxapp.repositories;

import com.spasorm.fxapp.models.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.LocalDate;

@Repository
public interface TransactionsRepository extends PagingAndSortingRepository<Transaction, Integer> {

    @Query("SELECT t FROM Transaction t WHERE t.id=?1 or (t.date >= ?2 and t.date<=?3)")
    Page<Transaction> findAllByIdOrDate(int id, LocalDate fromDate,LocalDate toDate, Pageable pageable);

    Page<Transaction> findAllByDate(LocalDate date, Pageable pageable);
}
