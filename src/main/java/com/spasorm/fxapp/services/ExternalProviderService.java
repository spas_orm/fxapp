package com.spasorm.fxapp.services;

import com.spasorm.fxapp.models.BaseRate;

public interface ExternalProviderService {

    public BaseRate getBaseRate();
}
