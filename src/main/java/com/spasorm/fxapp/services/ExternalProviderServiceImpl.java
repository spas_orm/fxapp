package com.spasorm.fxapp.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spasorm.fxapp.exceptions.ThirdPartyProviderConnectionException;
import com.spasorm.fxapp.models.BaseRate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;

@Service
public class ExternalProviderServiceImpl implements ExternalProviderService {

    private String serviceProviderUrl;

    @Autowired
    public ExternalProviderServiceImpl() {
    }

    @Override
    public BaseRate getBaseRate() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            URL url = new URL(serviceProviderUrl);
            BaseRate baseRate = mapper.readValue(url, BaseRate.class);
            return baseRate;
        } catch (Exception e) {
            throw new ThirdPartyProviderConnectionException();
        }
    }

    @Value("${rate.service.provider.url}")
    public void setServiceProviderUrl(String serviceProviderUrl) {
        this.serviceProviderUrl = serviceProviderUrl;
    }
}
