package com.spasorm.fxapp.services;

import com.spasorm.fxapp.models.BaseRate;
import com.spasorm.fxapp.models.ExchangeCurrencyPair;

import java.math.BigDecimal;

public interface RatesService {

    public BigDecimal getExchangeRate(String fromCurrency, String toCurrency);

}
