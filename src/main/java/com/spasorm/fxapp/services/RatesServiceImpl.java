package com.spasorm.fxapp.services;

import com.spasorm.fxapp.exceptions.CurrencyNotFoundException;
import com.spasorm.fxapp.models.BaseRate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class RatesServiceImpl implements RatesService {
    private static final String BASE_CURRENCY = "EUR";
    ExternalProviderService externalProviderService;

    @Autowired
    public RatesServiceImpl(ExternalProviderService externalProviderService) {
        this.externalProviderService=externalProviderService;
    }


    @Override
    public BigDecimal getExchangeRate(String fromCurrency, String toCurrency) {
        BaseRate baseRate = externalProviderService.getBaseRate();

        checkCurrencyExists(fromCurrency, baseRate);
        checkCurrencyExists(toCurrency, baseRate);

        if (fromCurrency.toUpperCase().equals(toCurrency.toUpperCase())) {
            return BigDecimal.ONE;
        }
        if (fromCurrency.toUpperCase().equals(BASE_CURRENCY)) {
            return baseRate.getRates().get(toCurrency.toUpperCase());
        }
        if (toCurrency.toUpperCase().equals(BASE_CURRENCY)) {
            BigDecimal one = BigDecimal.ONE;
            return one.divide(baseRate.getRates().get(fromCurrency.toUpperCase()), 6, RoundingMode.CEILING);
        }

        return baseRate.getRates().get(toCurrency.toUpperCase())
                .divide(baseRate.getRates().get(fromCurrency.toUpperCase()), 6, RoundingMode.CEILING);

    }

    private void checkCurrencyExists(String currency, BaseRate baseRate) {
        if (!baseRate.getRates().containsKey(currency.toUpperCase())) {
            throw new CurrencyNotFoundException();
        }
    }


}
