package com.spasorm.fxapp.services;

import com.spasorm.fxapp.models.Transaction;
import com.spasorm.fxapp.models.dtos.TransactionInputDTO;
import com.spasorm.fxapp.models.dtos.TransactionOutputDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

public interface TransactionsService {
    TransactionOutputDTO create(TransactionInputDTO transactionDto);

    Page<Transaction> getAll(Pageable pageable);

    Page<Transaction> getTransactionsByIdOrDate(int id, String date, Pageable pageable);

    Transaction getById(int id);

    Page<Transaction> getByDate(String date, Pageable pageable);
}
