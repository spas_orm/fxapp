package com.spasorm.fxapp.services;

import com.spasorm.fxapp.exceptions.AmountNotPositiveException;
import com.spasorm.fxapp.exceptions.InvalidDateFormatException;
import com.spasorm.fxapp.exceptions.MissingParametersException;
import com.spasorm.fxapp.exceptions.TransactionNotFoundException;
import com.spasorm.fxapp.models.Transaction;
import com.spasorm.fxapp.models.dtos.TransactionInputDTO;
import com.spasorm.fxapp.models.dtos.TransactionOutputDTO;
import com.spasorm.fxapp.models.mappers.TransactionMapper;
import com.spasorm.fxapp.repositories.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class TransactionsServiceImpl implements TransactionsService {
    private static final String ENTITY_NAME = "Transaction";

    TransactionsRepository transactionsRepository;
    RatesService ratesService;
    TransactionMapper transactionMapper;

    @Autowired
    public TransactionsServiceImpl(TransactionsRepository transactionsRepository,
                                   RatesService ratesService,
                                   TransactionMapper transactionMapper) {
        this.transactionsRepository = transactionsRepository;
        this.ratesService = ratesService;
        this.transactionMapper = transactionMapper;
    }

    @Override
    public TransactionOutputDTO create(TransactionInputDTO transactionInputDTO) {

        checkForZeroTransactionAmount(transactionInputDTO);


        Transaction transaction = transactionMapper.inputDtoToTransaction(transactionInputDTO);

        transaction.setRate(ratesService.getExchangeRate(transaction.getSourceCurrency(), transaction.getTargetCurrency()));
        transaction.setTargetAmount(transaction.getSourceAmount().multiply(transaction.getRate()));

        TransactionOutputDTO transactionOutputDTO = transactionMapper
                .transactionToOutDto(transactionsRepository.save(transaction));
        return transactionOutputDTO;
    }

    @Override
    public Page<Transaction> getAll(Pageable pageable) {
        return transactionsRepository.findAll(pageable);
    }

    @Override
    public Page<Transaction> getTransactionsByIdOrDate(int id, String date, Pageable pageable) {
        if (id != 0 && date == null) {
            LocalDate tomorrow = LocalDate.now().plusDays(1);
            return transactionsRepository.findAllByIdOrDate(id, tomorrow, tomorrow, pageable);
        }
        if (date != null) {
            try {
                LocalDate localDate = LocalDate.parse(date);
                return transactionsRepository.findAllByIdOrDate(id, localDate, localDate, pageable);
            } catch (DateTimeException dte) {
                throw new InvalidDateFormatException();
            }
        } else throw new MissingParametersException();
    }

    @Override
    public Transaction getById(int id) {
        Optional<Transaction> transaction = transactionsRepository.findById(id);
        if (!transaction.isPresent()) {
            throw new TransactionNotFoundException();
        }
        return transaction.get();
    }

    @Override
    public Page<Transaction> getByDate(String date, Pageable pageable) {
        try {
            return transactionsRepository.findAllByDate(LocalDate.parse(date), pageable);
        } catch (DateTimeException dte) {
            throw new InvalidDateFormatException();
        }
    }

    private void checkForZeroTransactionAmount(TransactionInputDTO transactionInputDTO) {
        if (transactionInputDTO.getSourceAmount().compareTo(BigDecimal.ZERO) <= 0) {
            throw new AmountNotPositiveException();
        }
    }

}
