DROP TABLE IF EXISTS transactions;

CREATE TABLE `transactions` (
                                `transaction_id` bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                `source_currency` varchar(3) NOT NULL,
                                `source_amount` DECIMAL(14,4) NOT NULL DEFAULT '0',
                                `target_currency` varchar(3) NOT NULL,
                                `target_amount` DECIMAL(14,4) NOT NULL DEFAULT '0',
                                `rate` DECIMAL(14,6) NOT NULL DEFAULT '0',
                                `date` DATE NOT NULL
);