package com.spasorm.fxapp;

import com.spasorm.fxapp.models.BaseRate;
import com.spasorm.fxapp.models.Transaction;
import com.spasorm.fxapp.models.dtos.TransactionInputDTO;
import com.spasorm.fxapp.models.dtos.TransactionOutputDTO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class Factory {

    public static final BigDecimal EUR_BGN_RATE = BigDecimal.valueOf(1.9754);
    public static final BigDecimal ONE_EUR_BGN_AMOUNT = BigDecimal.valueOf(1.9754);

    public static Transaction createTransaction() {
        Transaction transaction = new Transaction();

        transaction.setId(1);
        transaction.setSourceCurrency("EUR");
        transaction.setSourceAmount(BigDecimal.ONE);
        transaction.setTargetCurrency("BGN");
        transaction.setTargetAmount(ONE_EUR_BGN_AMOUNT);
        transaction.setRate(EUR_BGN_RATE);
        transaction.setDate(LocalDate.now());

        return transaction;
    }

    public static TransactionInputDTO createTransactionInputDto() {
        TransactionInputDTO transactionInputDTO = new TransactionInputDTO();

        transactionInputDTO.setSourceCurrency("EUR");
        transactionInputDTO.setTargetCurrency("BGN");
        transactionInputDTO.setSourceAmount(BigDecimal.ONE);

        return transactionInputDTO;
    }

    public static TransactionOutputDTO creatTransactionOutputDto() {
        TransactionOutputDTO transactionOutputDTO = new TransactionOutputDTO();

        transactionOutputDTO.setTargetAmount(ONE_EUR_BGN_AMOUNT);
        transactionOutputDTO.setTransactionId(1);

        return transactionOutputDTO;
    }

    public static Pageable createPageable() {
        return PageRequest.of(0, 5);
    }

    public static BaseRate createBaseRate() {
        BaseRate br = new BaseRate();
        br.setBase("EUR");
        br.setDate(LocalDate.now().toString());
        br.setSuccess(true);
        br.setTimestamp(123456789);
        Map<String, BigDecimal> rates = new HashMap<>();
        rates.put("EUR", BigDecimal.ONE);
        rates.put("BGN", EUR_BGN_RATE);
        rates.put("USD", BigDecimal.valueOf(1.084351));
        br.setRates(rates);
        return br;
    }
}
