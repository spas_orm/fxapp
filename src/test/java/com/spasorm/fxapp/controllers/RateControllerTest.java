package com.spasorm.fxapp.controllers;

import com.spasorm.fxapp.exceptions.CurrencyNotFoundException;
import com.spasorm.fxapp.services.RatesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.spasorm.fxapp.Factory.EUR_BGN_RATE;
import static com.spasorm.fxapp.exceptions.handlers.ControllerAdvisor.CURRENCY_NOT_AVAILABLE;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RateControllerTest {
    @Mock
    RatesService ratesService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void getRate_ShouldReturnStatusOK_WhenBodyOK() throws Exception {
        //Arrange
        Mockito.when(ratesService.getExchangeRate("EUR", "BGN")).thenReturn(EUR_BGN_RATE);

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/rate")
                .queryParam("fromCurrency", "EUR").queryParam("toCurrency", "BGN"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("1.9")))
                .andDo(print());
    }

    @Test
    public void getRate_ShouldReturnStatus404_WhenWrongCurrencyCode() throws Exception {
        //Arrange
        Mockito.when(ratesService.getExchangeRate("CCC", "BGN")).thenThrow(new CurrencyNotFoundException());

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/rate")
                .queryParam("fromCurrency", "CCC").queryParam("toCurrency", "BGN"))
                .andExpect(status().is(404))
                .andExpect(status().reason(CURRENCY_NOT_AVAILABLE))
                .andDo(print());
    }

}

