package com.spasorm.fxapp.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spasorm.fxapp.exceptions.*;
import com.spasorm.fxapp.models.Transaction;
import com.spasorm.fxapp.models.dtos.TransactionInputDTO;
import com.spasorm.fxapp.models.dtos.TransactionOutputDTO;
import com.spasorm.fxapp.services.TransactionsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.spasorm.fxapp.Factory.*;
import static com.spasorm.fxapp.exceptions.handlers.ControllerAdvisor.*;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TransactionsControllerTest {

    @Mock
    TransactionsService transactionsService;

    @Autowired
    MockMvc mockMvc;

//    @Autowired
//    GlobalExceptionLogger logger;

    @Test
    public void convertCurrency_ShouldReturnStatusOK_WhenBodyOK() throws Exception {
        //Arrange
        TransactionInputDTO transactionInputDTO = createTransactionInputDto();
        TransactionOutputDTO transactionOutputDTO = creatTransactionOutputDto();
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonObject = objectMapper.writeValueAsString(transactionInputDTO);

        Mockito.when(transactionsService.create(transactionInputDTO)).thenReturn(transactionOutputDTO);

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/convert").contentType(MediaType.APPLICATION_JSON_VALUE).content(jsonObject))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.targetAmount").value(1))
                .andDo(print());
    }

    @Test
    public void convertCurrency_ShouldReturnStatus400_WhenAmountNotPositive() throws Exception {
        //Arrange
        TransactionInputDTO transactionInputDTO = createTransactionInputDto();
        transactionInputDTO.setSourceAmount(BigDecimal.ZERO);
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonObject = objectMapper.writeValueAsString(transactionInputDTO);

        Mockito.when(transactionsService.create(transactionInputDTO))
                .thenThrow(new AmountNotPositiveException());

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/convert").contentType(MediaType.APPLICATION_JSON)
                .content(jsonObject))
                .andExpect(status().is(400))
                .andExpect(status().reason(INVALID_AMOUNT))
                .andDo(print());
    }

    @Test
    public void convertCurrency_ShouldReturnStatus404_WhenWrongCurrencyCode() throws Exception {
        //Arrange
        TransactionInputDTO transactionInputDTO = createTransactionInputDto();
        transactionInputDTO.setSourceCurrency("CCC");
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonObject = objectMapper.writeValueAsString(transactionInputDTO);

        Mockito.when(transactionsService.create(transactionInputDTO))
                .thenThrow(new CurrencyNotFoundException());

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/convert").contentType(MediaType.APPLICATION_JSON)
                .content(jsonObject))
                .andExpect(status().is(404))
                .andExpect(status().reason(CURRENCY_NOT_AVAILABLE))
                .andDo(print());
    }

    @Test
    public void getTransactionsForParameters_ShouldReturnStatusOK_WhenParametersOK() throws Exception {
        //Arrange
        Transaction transaction = createTransaction();
        List<Transaction> list = new ArrayList<>();
        list.add(transaction);
        Page<Transaction> transactionPage = new PageImpl<>(list);

        Mockito.when(transactionsService.getTransactionsByIdOrDate(anyInt(), anyString(), any(Pageable.class)))
                .thenReturn(transactionPage);

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/transaction")
                .queryParam("id","1").queryParam("date","2020-04-18"))
                .andExpect(status().isOk())
                .andDo(print());
    }

//    @Test
//    public void convertCurrency_ShouldReturnStatus404_WhenExternalProviderConnectionIssue() throws Exception {
//        //Arrange
//        TransactionInputDTO transactionInputDTO = createTransactionInputDto();
//        ObjectMapper objectMapper = new ObjectMapper();
//        String jsonObject = objectMapper.writeValueAsString(transactionInputDTO);
//
//        Mockito.when(ratesService.getExchangeRate(transactionInputDTO.getSourceCurrency(),transactionInputDTO.getTargetCurrency()))
//                .thenThrow(new ThirdPartyProviderConnectionException());
//
//        Mockito.when(transactionsService.create(transactionInputDTO))
//                .thenThrow(new ThirdPartyProviderConnectionException());
//
//        //Act, Assert
//        mockMvc.perform(MockMvcRequestBuilders
//                .post("/api/v1/convert").contentType(MediaType.APPLICATION_JSON)
//                .content(jsonObject))
//                .andExpect(status().is(424))
//                .andExpect(status().reason("Third party provider connection issue."))
//                .andDo(print());
//    }

    @Test
    public void getTransactionsForParameters_ShouldReturnStatus400_WhenWrongDateFormat() throws Exception {
        //Arrange
        Transaction transaction = createTransaction();
        List<Transaction> list = new ArrayList<>();
        list.add(transaction);

        Mockito.when(transactionsService.getTransactionsByIdOrDate(anyInt(), anyString(), any(Pageable.class)))
                .thenThrow(new InvalidDateFormatException());

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/transaction")
                        .queryParam("id","1").queryParam("date","2020-0418"))
                .andExpect(status().is(400))
                .andExpect(status().reason(INVALID_DATE_FORMAT))
                .andDo(print());
    }

    @Test
    public void getTransactionsForParameters_ShouldReturnStatus400_WhenNoParametersProvided() throws Exception {
        //Arrange
        Transaction transaction = createTransaction();

        Mockito.when(transactionsService.getTransactionsByIdOrDate(anyInt(), anyString(), any(Pageable.class)))
                .thenThrow(new MissingParametersException());

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/transaction").queryParam("id", ""))
                .andExpect(status().is(400))
                .andExpect(status().reason(MISSING_QUERY_PARAMETERS))
                .andDo(print());
    }

    @Test
    public void getById_ShouldReturnStatusOK_WhenTransactionExists() throws Exception {
        //Arrange
        Transaction transaction = createTransaction();

        Mockito.when(transactionsService.getById(anyInt()))
                .thenReturn(transaction);

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/transaction/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andDo(print());
    }

    @Test
    public void getById_ShouldReturnStatus404_WhenTransactionDoesntExist() throws Exception {
        //Arrange
        Mockito.when(transactionsService.getById(anyInt()))
                .thenThrow(new TransactionNotFoundException());

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/transaction/{id}", 0))
                .andExpect(status().is(404))
                .andExpect(status().reason(TRANSACTION_NOT_FOUND))
                .andDo(print());
    }

    @Test
    public void getByDate_ShouldReturnStatusOK_WhenDateFormatCorrect() throws Exception {
        //Arrange
        Pageable pageable = createPageable();
        Transaction transaction = createTransaction();
        List<Transaction> list = new ArrayList<>();
        list.add(transaction);
        Page<Transaction> transactionPage = new PageImpl<>(list);

        Mockito.when(transactionsService.getByDate(anyString(), any(Pageable.class)))
                .thenReturn(transactionPage);

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/transaction/date/{date}", LocalDate.now().toString()))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void getByDate_ShouldReturnStatus400_WhenWrongDateFormatUsed() throws Exception {
        //Arrange
        Pageable pageable = createPageable();
        Mockito.when(transactionsService.getByDate("18-04-2020", pageable))
                .thenThrow(new InvalidDateFormatException());

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/transaction/date/{date}", "18-04-2020"))
                .andExpect(status().is(400))
                .andExpect(status().reason(INVALID_DATE_FORMAT))
                .andDo(print());
    }
}
