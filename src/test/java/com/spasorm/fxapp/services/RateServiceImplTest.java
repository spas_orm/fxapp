package com.spasorm.fxapp.services;

import com.spasorm.fxapp.models.BaseRate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.spasorm.fxapp.Factory.EUR_BGN_RATE;
import static com.spasorm.fxapp.Factory.createBaseRate;

@RunWith(MockitoJUnitRunner.class)
public class RateServiceImplTest {

    @Mock
    ExternalProviderService externalProviderService;

    @InjectMocks
    RatesServiceImpl ratesService;

    @Test
    public void getExchangeRate_Should_ReturnRate_WhenCurrenciesValidBase() {
        //Arrange
        BaseRate baseRate = createBaseRate();
        Mockito.when(externalProviderService.getBaseRate()).thenReturn(baseRate);

        //Act
        BigDecimal resultRate = ratesService.getExchangeRate("EUR", "BGN");

        //Assert
        Assert.assertEquals(resultRate, EUR_BGN_RATE);
    }

    @Test
    public void getExchangeRate_Should_ReturnRate_WhenBaseIsTarget() {
        //Arrange
        BaseRate baseRate = createBaseRate();
        Mockito.when(externalProviderService.getBaseRate()).thenReturn(baseRate);

        //Act
        BigDecimal resultRate = ratesService.getExchangeRate("BGN", "EUR");

        //Assert
        Assert.assertEquals(resultRate, BigDecimal.valueOf(0.506227));
    }

    @Test
    public void getExchangeRate_Should_ReturnRate_WhenNotBase() {
        //Arrange
        BaseRate baseRate = createBaseRate();
        Mockito.when(externalProviderService.getBaseRate()).thenReturn(baseRate);

        //Act
        BigDecimal resultRate = ratesService.getExchangeRate("BGN", "USD");

        //Assert
        Assert.assertEquals(resultRate, BigDecimal.valueOf(0.548928));
    }

    @Test
    public void getExchangeRate_Should_ReturnRate_WhenCurrencySame() {
        //Arrange
        BaseRate baseRate = createBaseRate();
        Mockito.when(externalProviderService.getBaseRate()).thenReturn(baseRate);

        //Act
        BigDecimal resultRate = ratesService.getExchangeRate("EUR", "EUR");

        //Assert
        Assert.assertEquals(resultRate, BigDecimal.ONE);
    }

}
