package com.spasorm.fxapp.services;

import com.spasorm.fxapp.exceptions.AmountNotPositiveException;
import com.spasorm.fxapp.exceptions.CurrencyNotFoundException;
import com.spasorm.fxapp.exceptions.TransactionNotFoundException;
import com.spasorm.fxapp.exceptions.MissingParametersException;
import com.spasorm.fxapp.models.Transaction;
import com.spasorm.fxapp.models.dtos.TransactionInputDTO;
import com.spasorm.fxapp.models.dtos.TransactionOutputDTO;
import com.spasorm.fxapp.models.mappers.TransactionMapper;
import com.spasorm.fxapp.repositories.TransactionsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.spasorm.fxapp.Factory.*;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionsServiceImplTest {

    @Mock
    TransactionsRepository transactionsRepository;

    @Mock
    TransactionMapper transactionMapper;

    @Mock
    RatesService ratesService;

    @InjectMocks
    TransactionsServiceImpl transactionsService;

    @Test
    public void create_Should_ReturnTransaction_WhenSuccessful() {
        //Arrange
        Transaction transaction = createTransaction();
        TransactionInputDTO transactionInputDTO = createTransactionInputDto();
        TransactionOutputDTO transactionOutputDTO = creatTransactionOutputDto();
        Mockito.when(transactionsRepository.save(transaction)).thenReturn(transaction);
        Mockito.when(transactionMapper.inputDtoToTransaction(transactionInputDTO)).thenReturn(transaction);
        Mockito.when(ratesService.getExchangeRate(anyString(), anyString())).thenReturn(EUR_BGN_RATE);
        Mockito.when(transactionMapper.transactionToOutDto(transaction)).thenReturn(transactionOutputDTO);
        //Act
        TransactionOutputDTO resultTransaction = transactionsService.create(transactionInputDTO);

        //Assert
        Assert.assertSame(transactionOutputDTO, resultTransaction);
    }

    @Test(expected = CurrencyNotFoundException.class) //Assert
    public void create_Should_Throw_When_getExchangeRate_Throws() {
        //Arrange
        Transaction transaction = createTransaction();
        TransactionInputDTO transactionInputDTO = createTransactionInputDto();
        Mockito.when(transactionMapper.inputDtoToTransaction(transactionInputDTO)).thenReturn(transaction);
        Mockito.when(ratesService.getExchangeRate(anyString(), anyString())).thenThrow(new CurrencyNotFoundException());
        //Act
        TransactionOutputDTO resultTransaction = transactionsService.create(transactionInputDTO);
    }

    @Test(expected = AmountNotPositiveException.class) //Assert
    public void create_Should_Throw_WhenTransactionAmountNotPositive() {
        //Arrange
        Transaction transaction = createTransaction();
        TransactionInputDTO transactionInputDTO = createTransactionInputDto();
        transactionInputDTO.setSourceAmount(BigDecimal.ZERO);
        //Act
        TransactionOutputDTO resultTransaction = transactionsService.create(transactionInputDTO);
    }

    @Test
    public void getById_Should_ReturnTransaction_WhenIdValid() {
        //Arrange
        Transaction expectedTransaction = createTransaction();
        Mockito.when(transactionsRepository.findById(anyInt())).thenReturn(java.util.Optional.of(expectedTransaction));

        //Act
        Transaction resultTransaction = transactionsService.getById(anyInt());

        //Assert
        Assert.assertSame(resultTransaction, expectedTransaction);
    }

    @Test(expected = TransactionNotFoundException.class) //Assert
    public void getById_ShouldThrow_WhenTransactionDoesntExist() {
        //Arrange
        Mockito.when(transactionsRepository.findById(anyInt())).thenReturn(Optional.empty());

        //Act
        Transaction resultTransaction = transactionsService.getById(anyInt());
    }

    @Test
    public void getAll_Should_ReturnPageTransactions_WhenSuccessful() {
        //Arrange
        Pageable pageable = createPageable();
        Page<Transaction> transactionPage = new PageImpl<>(new ArrayList<>());
        Mockito.when(transactionsRepository.findAll(pageable)).thenReturn(transactionPage);

        //Act
        Page<Transaction> result = transactionsService.getAll(pageable);

        //Assert
        Assert.assertSame(transactionPage, result);
    }

    @Test
    public void getTransactionsByIdOrDate_Should_ReturnPageTransaction_WhenAllParametersProvided() {
        //Arrange
        Pageable pageable = createPageable();
        Transaction transaction = createTransaction();
        List<Transaction> list = new ArrayList<>();
        list.add(transaction);
        Page<Transaction> transactionPage = new PageImpl<>(list);
        int id = 1;
        String date = LocalDate.now().toString();
        Mockito.when(transactionsRepository.findAllByIdOrDate(anyInt(),any(LocalDate.class),any(LocalDate.class),any(Pageable.class)))
                .thenReturn(transactionPage);

        //Act
        Page<Transaction> result = transactionsService.getTransactionsByIdOrDate(id,date,pageable);

        //Assert
        Assert.assertSame(transactionPage, result);
    }

    @Test
    public void getTransactionsByIdOrDate_Should_ReturnPageTransaction_WhenOnlyDateProvided() {
        //Arrange
        Pageable pageable = createPageable();
        Transaction transaction = createTransaction();
        List<Transaction> list = new ArrayList<>();
        list.add(transaction);
        Page<Transaction> transactionPage = new PageImpl<>(list);
        int id = 1;
        String date = LocalDate.now().toString();
        Mockito.when(transactionsRepository.findAllByIdOrDate(anyInt(),any(LocalDate.class),any(LocalDate.class),any(Pageable.class)))
                .thenReturn(transactionPage);

        //Act
        Page<Transaction> result = transactionsService.getTransactionsByIdOrDate(id,date,pageable);

        //Assert
        Assert.assertSame(transactionPage, result);
    }

    @Test
    public void getTransactionsByIdOrDate_Should_ReturnPageTransaction_WhenOnlyIdProvided() {
        //Arrange
        Pageable pageable = createPageable();
        Transaction transaction = createTransaction();
        List<Transaction> list = new ArrayList<>();
        list.add(transaction);
        Page<Transaction> transactionPage = new PageImpl<>(list);
        int id = 1;
        String date = null;
        Mockito.when(transactionsRepository.findAllByIdOrDate(anyInt(),any(LocalDate.class),any(LocalDate.class),any(Pageable.class)))
                .thenReturn(transactionPage);

        //Act
        Page<Transaction> result = transactionsService.getTransactionsByIdOrDate(id,date,pageable);

        //Assert
        Assert.assertSame(transactionPage, result);
    }

    @Test(expected = MissingParametersException.class) //Assert
    public void getTransactionsByIdOrDate_Should_Throw_WhenNoParametersProvided() {
        //Arrange
        Pageable pageable = createPageable();
        int id = 0;
        String date=null;
        //Act
        Page<Transaction> result = transactionsService.getTransactionsByIdOrDate(id,date,pageable);
    }

    @Test
    public void getByDate_Should_ReturnPageTransactions_WhenSuccessful() {
        //Arrange
        Pageable pageable = createPageable();
        Page<Transaction> transactionPage = new PageImpl<>(new ArrayList<>());

        LocalDate date = LocalDate.now();
        String dateString = date.toString();
        Mockito.when(transactionsRepository.findAllByDate(date,pageable)).thenReturn(transactionPage);

        //Act
        Page<Transaction> result = transactionsService.getByDate(dateString,pageable);

        //Assert
        Assert.assertSame(transactionPage, result);
    }
}
